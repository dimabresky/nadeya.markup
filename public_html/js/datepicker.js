
/**
 * bootstrap datepicker init
 */
$(document).ready(function () {
    
    $('.ndatepicker').datepicker({
        language: __nconfig.locale,
        autoclose: true
    });
});
