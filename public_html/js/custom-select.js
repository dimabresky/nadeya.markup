
/**
 * custom select
 * 
 * trigger event "custom-select:change"
 */
$(document).ready(function () {

    var $custom_selects = $(".custom-select");

    function set_select_values_position(select_values, select_input) {
        select_values.css({
            width: `${select_input.outerWidth()}px`,
            position: 'absolute',
            top: `${select_input.outerHeight()}px`
        });
    }

    function interator(callback) {
        $custom_selects.each(function () {

            var $context = $(this);
            callback($context);
        });
    }

    interator(function ($context) {

        set_select_values_position($context.find('.custom-select__values'), $context.find(".custom-select__input"));

        $context.find(".custom-select__input").on("click", function (e) {

            $context.find('.custom-select__values').removeClass('hidden');
            e.stopPropagation();
        });
        $context.find(".custom-select__value").on("click", function () {

            var $this = $(this);
            var value = $(this).data('value');
            $context.find(".custom-select__value-active").removeClass('custom-select__value-active');
            $this.addClass('custom-select__value-active');
            $context.find('input[type=hidden]').val(value);
            $context.find('.custom-select__input').text(value);
            $context.trigger("custom-select:change");
            $context.find('.custom-select__values').addClass('hidden');
        });
    });

    $("body").on('click', function () {
        $custom_selects.find('.custom-select__values').addClass('hidden');
    });

    $(window).resize(function () {
        interator(function ($context) {
            set_select_values_position($context.find('.custom-select__values'), $context.find(".custom-select__input"));
        });
    });
});
