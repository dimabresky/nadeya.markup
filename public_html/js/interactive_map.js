
/**
 * interactive map
 */
$(document).ready(function () {
    
    var interactive_map = document.querySelector(".interactive-map__container");
    var scale = 0.7;
    
    $(".scale__plus").on("click", function () {
        if (scale < 0.9) {
            scale = scale + 0.1;
            interactive_map.style.transform = `scale(${scale})`;
        }
    });
    
    $(".scale__minus").on("click", function () {
        if (scale > 0.4) {
            scale = scale - 0.1;
            interactive_map.style.transform = `scale(${scale})`;
        }
    });
});