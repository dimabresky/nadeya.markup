
/**
 * slider
 */

$(document).ready(function () {

    if (typeof $.fn.owlCarousel === "function") {

        $('.owl-carousel').owlCarousel({
            center: true,
            loop: true,
            autoWidth: true,
            items: 4
        });

    }

});