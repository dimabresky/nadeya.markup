
/**
 * faq
 */
$(document).ready(function () {
    $(".faq__thumb-title").on("click", function () {
        $(this).toggleClass('faq__thumb-title_disabled');
        $(this).closest(".faq__thumb-wrapper").find(".faq__thumb-answer").toggleClass("faq__thumb-answer_active");
    });

    $(".faq__thumb-answer").on("click", function () {
        $(this).toggleClass('faq__thumb-answer_active');
        $(this).closest(".faq__thumb-wrapper").find(".faq__thumb-title").toggleClass("faq__thumb-title_disabled");
    });
});